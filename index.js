const Hapi = require("hapi");
const loadPlugins = require("./lib/loadPlugins.js");
const cron = require("node-cron");
const models = require("./model");
const chalk = require("chalk");
const { scheduleJobs } = require("./jobs/schedule");
// const reportErrors = require('./cron/reportErrors');
const { rawquery } = models;
const { Logger } = require("./utils/logger");

const server = new Hapi.Server({
  host: "0.0.0.0",
  port: process.env.port || 3000,
  // debug: { request: ['error'] },
  routes: {
    cors: {
      origin: ["*"],
      additionalHeaders: [
        "provider_name",
        "token",
        "accesskeyid",

        "content-type",
        "secretaccesskey",
        "sessiontoken",
        "refresh_token",
        "platform"
      ]
    }
  }
});

const validate = async (decoded, request) => {
  const { email } = decoded;
  const Query = `SELECT COUNT(email) from users where email='${email}';`;
  const [result, _] = await rawquery(Query);
  if (parseInt(result[0].count) > 0) {
    return { isValid: true };
  } else {
    return { isValid: false };
  }
};
const startServer = async server => {
  try {
    server.events.on("route", route =>
      console.log(
        chalk.green(
          `Route Loaded-> Method:${chalk.yellow(
            route.method.toUpperCase()
          )} ${chalk.cyan.underline(route.path)}`
        )
      )
    );
    await models.createModels();
    await loadPlugins.registerPlugins(server);
    await server.start();
    server.log(["startup"], `Environement: ${process.env.NODE_ENV}`);
    server.log(["startup"], `Server running at ${server.info.uri}`);
    scheduleJobs();

    server.auth.strategy("jwt", "jwt", {
      key: "ELEBELEDUDUBHAATU",
      validate,
      verifyOptions: { algorithms: ["HS256"] }
    });

    server.auth.default("jwt");
  } catch (err) {
    /* handle error */
    console.log(err);
    server.log(["startup", "error"], `Server start error ${err}`);
    process.exit(1);
  }
};

startServer(server);
