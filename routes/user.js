const Boom = require("boom");
const Axios = require("axios");
const Config = require("../lib/config")("dev");
const { insert, read } = require("../model");
const jwt = require("jsonwebtoken");

module.exports = {
  method: "GET",
  path: "/user",
  handler: async (req, h) => {
    console.log(req);
    if (req.headers.authorization) {
      const payload = jwt.decode(req.headers.authorization);
      const { email } = payload;
      const UserObj = await read({ dbName: "user", where: { email: email } });

      return {
        ...UserObj.get({ plain: true })
      };
    } else {
      return Boom.badData("No Token");
    }
  }
};
