const BaseJoi = require("joi");
const Extension = require("joi-date-extensions");
const JOI = BaseJoi.extend(Extension);
const { getIATACodes } = require("../lib/getIATACodes");

const R = require("ramda");
// const Flight = require("../model/flights").Flight;

const Boom = require("boom");
const handler = async (req, h) => {
  try {
    const {
      query: { city }
    } = req;
    return await getIATACodes(city);
  } catch (error) {
    throw Boom.badData(error.message);
  }
};

module.exports = {
  method: "GET",
  path: "/iatacodes",
  handler,
  config: {
    auth: false,
    validate: {
      query: {
        city: JOI.string()
          .min(3)
          .required()
      }
    }
  }
};
