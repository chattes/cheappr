const { Logger } = require("../utils/logger");
const dayjs = require("dayjs");

module.exports = {
  method: "GET",
  path: "/get",

  handler: async (req, h) => {
    text = `
    *We are trying hard to make an App.*
    _This is just the beginning_
    
    We will post our messages and Logs here!!
    Ciyao

    Time is ${dayjs().format("DD-MM-YYYY HH mm")}
    
    `;
    Logger(text)
      .then(data => console.log(data))
      .catch(error => console.log(error));
    return "All Good";
  },

  config: {
    auth: false
  }
};
