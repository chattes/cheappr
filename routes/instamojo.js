const JOI = require("joi");
const dayjs = require("dayjs");
const { insert, creditCoins } = require("../model");
// const Axios = require("axios");

const request = require("request");
const MOJOPROD = {
  API_KEY: "ff32d6a3d6f65f7bb0ec5a98d8f53982",
  AUTH_TOKEN: "f56c553d35270b55b58f6fd2cf22c52d",
  SALT: "1a8349a393144f2d961636cb0ac53ad3"
};
const MOJOTEST = {
  API_KEY: "test_758aa50389a22276da95c1b2b63",
  AUTH_TOKEN: "test_f5e91bf35ac794e2b25d8065a78",
  SALT: "476ede01d1fd46bfb93bce19f91d4dac"
};
const MOJOPRODURL = "https://www.instamojo.com/api/1.1/payment-requests/";
const MOJOTESTURL = "https://test.instamojo.com/api/1.1/payment-requests/";
const Boom = require("boom");

const getPaymentURL = async (req, h) => {
  const { params } = req;

  var options = {
    method: "POST",
    url: MOJOPRODURL,
    headers: {
      "content-type": "application/json",
      "x-auth-token": MOJOPROD.AUTH_TOKEN,
      "x-api-key": MOJOPROD.API_KEY
    },
    body: {
      amount: "50.00",
      buyer_name: params.name,
      email: params.email,
      phone: "+919999999999",
      send_email: false,
      send_sms: false,
      purpose: "CHEAPPR 50",
      expires_at: dayjs().add(5, "minutes"),
      allow_repeated_payments: false,
      webhook: "https://api.cheappr.io/mojoCallBack",
      redirect_url: "https://cheappr.io/paymentsuccess"
    },
    json: true
  };

  const paymenturl = await new Promise((resolve, reject) => {
    request(options, (error, response, body) => {
      if (error) return reject(error.message);
      return resolve(body);
    });
  }).catch(error => {
    throw Boom.badData();
  });

  if (paymenturl) return paymenturl.payment_request.longurl;

  //   const requestURL = await Axios({
  //     request: "POST",
  //     url: "https://test.instamojo.com/api/1.1/payment-requests/",
  //     data: {
  //       amount: "50.00",
  //       buyer_name: params.name,
  //       email: params.email,
  //       phone: "+919999999999",
  //       send_email: false,
  //       send_sms: false,
  //       purpose: "CHEAPPR 50"
  //     },
  //     headers: {
  //       "X-Api-Key": MOJOTEST.API_KEY,
  //       "X-Auth-Token": MOJOTEST.AUTH_TOKEN,
  //       "content-type": "application/json"
  //     }
  //   }).catch(error => {
  //     console.log(error);
  //     throw Boom.badData(error.message);
  //   });
};
const paymentDetails = (request, h) => {
  const { payload } = request;
  if (payload.status === "Credit") {
    insert("transactions", {
      userEmail: payload.buyer,
      transaction_detail: payload
    }).catch(error => console.log(error.message));

    creditCoins(payload.buyer).catch(error => console.log(error.message));
  }

  return 200;
};

module.exports = [
  {
    method: "get",
    path: "/paymentURL/{name}/{email}",
    handler: getPaymentURL,
    options: {
      validate: {
        params: {
          name: JOI.string().required(),
          email: JOI.string().required()
        }
      }
    }
  },
  {
    method: "post",
    path: "/mojoCallBack",
    handler: paymentDetails,
    config: {
      auth: false
    }
  }
];
