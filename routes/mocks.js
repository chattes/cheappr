const BaseJoi = require("joi");
const Extension = require("joi-date-extensions");
const JOI = BaseJoi.extend(Extension);
const dayjs = require("dayjs");

const { dealKiwi } = require("../lib/deal_finder_kiwi");
const { dealMomondo } = require("../lib/deal_finder_momondo");

const R = require("ramda");
// const Flight = require("../model/flights").Flight;

const dealFinder = async (req, h) => {
  dealKiwi();
  return 200;
};
const dealFinderMomondo = async (req, h) => {
  dealMomondo("CCU");
  return 200;
};

module.exports = [
  {
    method: "GET",
    path: "/dealfinder",
    handler: dealFinder,
    config: {
      auth: false
    }
  },
  {
    method: "GET",
    path: "/dealfinderm",
    handler: dealFinderMomondo,
    config: {
      auth: false
    }
  }
];
