const Boom = require("boom");
const Axios = require("axios");
const Config = require("../lib/config")("dev");
const { OAuth2Client } = require("google-auth-library");
const client = new OAuth2Client(Config.google.google_client_id);
const { insert } = require("../model");
const jwt = require("jsonwebtoken");
const { APP_CONSTANTS } = require("../lib/constants");
const dayjs = require("dayjs");

const signToken = user => {
  return jwt.sign(user, "ELEBELEDUDUBHAATU", {
    expiresIn: "30d",
    issuer: "CHEAPPR_APP"
  });
};

module.exports = {
  method: ["GET", "POST"],
  path: "/google",

  handler: async (req, h) => {
    try {
      console.log("Callback Response from google", req);
      if (req.headers.authorization) {
        // const token = req.headers.authorization;
        // const ticket = await client.verifyIdToken({
        //   idToken: token,
        //   audience: Config.google.google_client_id // Specify the CLIENT_ID of the app that accesses the backend
        //   // Or, if multiple clients access the backend:
        //   //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
        // });
        // const payload = ticket.getPayload();
        const { email } = req.payload;
        if (!email || email === "") {
          throw new Error("Cannot Signup");
        }
        const UserObj = await insert(
          "user",
          { email: email },
          {
            firstName: "CHEAPPR",
            lastName: "USER",
            fullName: "ONBOARD",
            profilePic: "",
            membershipType: APP_CONSTANTS.MEMBER_FREE,
            role: email === "gooner.sourav@gmail.com" ? "ADMIN" : "USER",
            lastDeal: dayjs()
              .subtract(1, "day")
              .toDate(),
            validFrom: dayjs().toDate(),
            validTill: dayjs()
              .add(5, "year")
              .toDate()
          }
        );

        const { created, ...user } = UserObj;
        if (created) {
          // insert("cheappr_coins", { userEmail: payload.email, coins: 5 });
        }
        const jwttoken = signToken(user);
        return {
          jwttoken,
          created
        };
      } else {
        Boom.unauthorized("Login Failed");
      }
    } catch (e) {
      Boom.badImplementation();
    }
  },
  config: {
    auth: false
  }
};
