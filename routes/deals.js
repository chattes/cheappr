const BaseJoi = require("joi");
const Extension = require("joi-date-extensions");
const JOI = BaseJoi.extend(Extension);
const R = require("ramda");
const { read } = require("../model");
const { publishDealsForUsers } = require("../lib/publishDealsForUsers");
// const Flight = require("../model/flights").Flight;
const Boom = require("boom");
const dayjs = require("dayjs");

const publishedDealsHandler = async (req, h) => {
  try {
    const {
      query: { page, limit }
    } = req;

    const offset = (page - 1) * limit;

    const deals = await read({
      dbName: "published_deals",
      order: [["createdAt", "DESC"]],
      limit: 10,
      offset
    });

    req.totalCount = deals.count;

    const models = deals.rows;
    const data = models.map(model => model.get({ plain: true }));
    const published_data = data.map(deal => {
      const {
        cityFrom,
        cityTo,
        countryFrom,
        countryTo,
        link,
        nights,
        price,
        usual_price,
        premium,
        valid_to,
        image_url
      } = deal;

      const expired = dayjs().isAfter(dayjs(valid_to), "day");
      console.log("Sourav Logging:::: Expired", expired);
      return {
        cityFrom,
        cityTo,
        countryFrom,
        countryTo,
        link,
        nights,
        price,
        usual_price,
        premium,
        expired,
        image_url
      };
    });
    return published_data;
  } catch (error) {
    throw Boom.badData(error.message);
  }
};
const dealsHandler = async (req, h) => {
  try {
    const {
      query: { page, limit }
    } = req;

    const offset = (page - 1) * limit;

    const deals = await read({
      dbName: "deals",
      order: [["id", "DESC"]],
      limit: 10,
      offset
    });

    req.totalCount = deals.count;

    const models = deals.rows;
    const data = models.map(model => model.get({ plain: true }));

    return data;
  } catch (error) {
    throw Boom.badData(error.message);
  }
};

const publishHandler = async (req, h) => {
  try {
    console.log("Sourav Logging:::: Request", req.payload);
    await publishDealsForUsers(req.payload);
    return 200;
  } catch (error) {
    throw Boom.badData(error.message);
  }
};

module.exports = [
  {
    method: "GET",
    path: "/alldeals",
    handler: dealsHandler,
    config: {
      plugins: {
        pagination: {
          enabled: true
        }
      },
      validate: {
        query: {
          limit: JOI.number()
            .default(10)
            .min(1)
            .max(30),
          page: JOI.number().positive()
        }
      }
    }
  },
  {
    method: "GET",
    path: "/viewpub",
    handler: publishedDealsHandler,
    config: {
      plugins: {
        pagination: {
          enabled: true
        }
      },
      validate: {
        query: {
          limit: JOI.number()
            .default(10)
            .min(1)
            .max(30),
          page: JOI.number().positive()
        }
      }
    }
  },
  {
    method: "POST",
    path: "/publish",
    handler: publishHandler,
    config: {
      validate: {
        payload: {
          cityFrom: JOI.string().required(),
          cityTo: JOI.string().required(),
          countryFrom: JOI.string().required(),
          countryTo: JOI.string().required(),
          link: JOI.string().required(),
          nights: JOI.number().required(),
          price: JOI.number().required(),
          usual_price: JOI.number().required(),
          premium: JOI.boolean(),
          notes: JOI.string(),
          image_url: JOI.string(),
          when: JOI.string(),
          airlines: JOI.string()
        }
      }
    }
  }
];
