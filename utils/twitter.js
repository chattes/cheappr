const Twitter = require("twitter");
const R = require("ramda");
const dayjs = require("dayjs");

const client = new Twitter({
  consumer_key: process.env.CONSUMER_KEY,
  consumer_secret: process.env.CONSUMER_SECRET,
  access_token_key: process.env.ACCESS_TOKEN_KEY,
  access_token_secret: process.env.ACCESS_TOKEN_SECRET
});

tweet = message => {
  client
    .post("statuses/update", { status: message })
    .then(tweet => console.log("Posted on Twitter", tweet))
    .catch(error => console.log("Cannot post on Twitter"));
};

shouldTweet = async () => {
  const params = { screen_name: "Cheappr3", count: 1 };

  return client
    .get("statuses/user_timeline", params)
    .then(tweets => {
      console.log(tweets);
      if (tweets.length > 0) {
        const { created_at = null } = tweets[0];
        if (created_at) {
          const diff_days = dayjs().diff(dayjs(created_at), "day");
          console.log(diff_days);
          return diff_days > 0;
        } else {
          return false;
        }
      } else {
        return false;
      }
    })
    .catch(error => console.log(error));
};

module.exports = {
  tweet,
  shouldTweet
};
