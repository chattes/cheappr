const R = require("ramda");
const axios = require("axios");

const Logger = text => {
  if (process.env.NODE_ENV !== "production") return;
  axios
    .request({
      method: "post",
      url: `https://hooks.slack.com/services/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`,
      headers: {
        Accept: "application/json"
      },
      data: {
        text
      },
      timeout: 20000
    })
    .catch(error => console.log("Cannot Log to Slack"));
  return;
};

module.exports = {
  Logger
};
