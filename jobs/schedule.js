const { dealKiwi } = require("../lib/deal_finder_kiwi");
const { dealMomondo } = require("../lib/deal_finder_momondo");
const { dealKayak } = require("../lib/deal_finder_kayak");
const cron = require("node-cron");

const scheduleJobs = () => {
  // Run cheappr every 50 minutes
  cron.schedule(
    "0 1 * * *",
    () => {
      dealKiwi();
    },
    {
      scheduled: true,
      timezone: "Europe/Prague"
    }
  );
  cron.schedule(
    "0 2 * * *",
    () => {
      dealKayak("VTZ");
    },
    {
      scheduled: true,
      timezone: "Europe/Prague"
    }
  );
  cron.schedule(
    "15 4 * * *",
    () => {
      dealKayak("DEL");
    },
    {
      scheduled: true,
      timezone: "Europe/Prague"
    }
  );
  cron.schedule(
    "0 5 * * *",
    () => {
      dealMomondo("CCU");
    },
    {
      scheduled: true,
      timezone: "Europe/Prague"
    }
  );
  cron.schedule(
    "0 7 * * *",
    () => {
      dealMomondo("BLR");
    },
    {
      scheduled: true,
      timezone: "Europe/Prague"
    }
  );
  cron.schedule(
    "30 9 * * *",
    () => {
      dealMomondo("MAA");
    },
    {
      scheduled: true,
      timezone: "Europe/Prague"
    }
  );
  cron.schedule(
    "30 11 * * *",
    () => {
      dealMomondo("COK");
    },
    {
      scheduled: true,
      timezone: "Europe/Prague"
    }
  );

  cron.schedule(
    "00 14 * * *",
    () => {
      dealMomondo("HYD");
    },
    {
      scheduled: true,
      timezone: "Europe/Prague"
    }
  );
  cron.schedule(
    "30 17 * * *",
    () => {
      dealMomondo("AMD");
    },
    {
      scheduled: true,
      timezone: "Europe/Prague"
    }
  );
  cron.schedule(
    "15 18 * * *",
    () => {
      dealKayak("BOM");
    },
    {
      scheduled: true,
      timezone: "Europe/Prague"
    }
  );
  cron.schedule(
    "30 20 * * *",
    () => {
      dealMomondo("ATQ");
    },
    {
      scheduled: true,
      timezone: "Europe/Prague"
    }
  );
};

cron.schedule(
  "15 23 * * *",
  () => {
    dealKayak("COK");
  },
  {
    scheduled: true,
    timezone: "Europe/Prague"
  }
);

module.exports = {
  scheduleJobs
};
