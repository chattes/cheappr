const {
  getDateRange,
  fetchFlightDetails,
  getLowestFare,
  getMeanFare
} = require("../lib/flights");
const { expect } = require("chai");
const dayjs = require("dayjs");

describe("Date Ranges Test", function() {
  describe("Adds Seven Days from Date Provided - YYYYMMDD", function() {
    it("Should Return 7 consecutive days as Array", function() {
      const dates = getDateRange("20180901");
      expect(dates)
        .to.be.an("array")
        .of.length(7);
    });
    it("Should Return 7 consecutive days as Array - YYYY-MM-DD", function() {
      const dates = getDateRange("2018-09-01");
      expect(dates)
        .to.be.an("array")
        .of.length(7);
    });
  });
  describe("Date format", function() {
    it("Date Format is YYYYMMDD", function() {
      const dates = getDateRange("20180901");
      expect(dates)
        .to.include("20180907")
        .to.not.include("20180908");
    });
  });
});

// describe("API TEST", function() {
//   describe("Calling GOIBIBO API", function() {
//     it("Should Return payload with Status 200", async function() {
//       this.timeout(20000);
//       const [err, flights_data] = await fetchFlightDetails(
//         "CCU",
//         "BLR",
//         dayjs()
//           .add(1, "days")
//           .format("YYYYMMDD")
//       );

//       expect(flights_data.status).to.equal(200);
//       // console.log(flights_data.data.data.onwardflights);
//     });
//     it("Should Return Flight Results", async function() {
//       this.timeout(20000);
//       const [err, flights_data] = await fetchFlightDetails(
//         "CCU",
//         "BLR",
//         dayjs()
//           .add(1, "days")
//           .format("YYYYMMDD")
//       );

//       expect(flights_data.data.data.onwardflights.length).to.be.greaterThan(0);
//       // console.log(flights_data.data.data.onwardflights);
//     });
//   });
// });

describe("Cheapest Fares Test", function() {
  it("Should return the Cheapest Fare", function() {
    const fares = [
      {
        item: 1,
        fare: {
          grossamount: 100
        }
      },
      {
        item: 2,
        fare: {
          grossamount: 300
        }
      },
      {
        item: 3,
        fare: {
          grossamount: 50
        }
      }
    ];
    expect(getLowestFare(fares).item).to.equal(3);
  });
  it("Should Return the Mean of all Fares", function() {
    const fares = [100, 200, 300];
    expect(getMeanFare(fares)).to.equal(200);
  });
});
