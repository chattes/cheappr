const {
  getAllFares,
  getLowFaresByDay,
  createCTDirectLink,
  getLowestFare
} = require("../lib/cleartrip");
const { expect } = require("chai");
const ctData = require("./mocks/ct.json");
const dayjs = require("dayjs");

describe("Cleartrip Tests", function() {
  it("Should respond with 200", async function() {
    this.timeout(20000);
    const input = {
      from: "CCU",
      to: "BLR",
      start_date: "20190401",
      end_date: "20190431"
    };
    const response = await getAllFares(input);
    expect(response.status).to.equal(200);
  });

  it("Should get a collection of Lowest Fares by Date", function() {
    const LowFaresByDay = getLowFaresByDay({ data: ctData });
    expect(LowFaresByDay.length).to.be.greaterThan(0);
  });
  it("All Prices in Collection should be valid Price", function() {
    const LowFaresByDay = getLowFaresByDay({ data: ctData });
    LowFaresByDay.forEach(fare => {
      expect(fare.price).to.satisfy(Number.isInteger);
    });
  });
  it("All Prices in Collection should have a Valid Date", function() {
    const LowFaresByDay = getLowFaresByDay({ data: ctData });
    LowFaresByDay.forEach(fare => {
      expect(dayjs(fare.date).isValid()).to.equal(true);
    });
  });
  it("Should return a Direct Link", function() {
    const link = createCTDirectLink({
      from: "CCU",
      to: "BLR",
      date: "20180903"
    });
    expect(link).to.equal(
      "https://www.cleartrip.com/flights/results?from=CCU&to=BLR&depart_date=03/09/2018&adults=1&childs=0&infants=0&dep_time=0&class=Economy&airline=&carrier=&x=57&y=16&flexi_search=no&page=loaded"
    );
  });

  it("Should get the lowest fare for all the dates", function() {
    const fares = [
      {
        price: 500,
        date: "2018-09-02",
        link: "test"
      },
      {
        price: 700,
        date: "2018-09-03",
        link: "test"
      },
      {
        price: 200,
        date: "2018-09-10",
        link: "test"
      }
    ];

    const fare = getLowestFare(fares);
    expect(fare[0]).to.deep.equal({
      price: 200,
      date: "2018-09-10",
      link: "test"
    });
  });
});
