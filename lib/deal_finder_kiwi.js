const Axios = require("axios");
const fs = require("fs").promises;
const R = require("ramda");
const dayjs = require("dayjs");
const Boom = require("boom");
const { destroy, bulkInsert, read } = require("../model");
const { Logger } = require("../utils/logger");
const { tweet, shouldTweet } = require("../utils/twitter");
const { priceIndicator } = require("../Rules/priceList");
const priceListNew = require("../config/pricelistnew.json");
let browser;

const Request = Axios.create({
  // baseURL: "https://api.skypicker.com",
  timeout: 20000
});

const deepLinks = ({ flyFrom, flyTo, dtimetrip1, dtimetrip2 }) => {
  const dep_date1_google = dayjs.unix(dtimetrip1).format("YYYY-MM-DD");
  const dep_date2_google = dayjs()
    .unix(dtimetrip2)
    .format("YYYY-MM-DD");
  const deep_link_google = `https://www.google.com/flights?hl=en#flt=${flyFrom}.${flyTo}.${dep_date1_google}*${flyTo}.${flyFrom}.${dep_date2_google};c:INR;e:1;sd:1;t:f`;
  return {
    deep_link_google
  };
};

const dealKiwi = async () => {
  try {
    const locationUrl =
      "https://api.skypicker.com/locations?type=top_destinations&term=IN&locale=en-US&limit=300&sort=sort=name&active_only=true&source_popularity=searches&fallback_popularity=";
    const location_proxy = `http://api.scraperapi.com?api_key=09f8e0c1461b2d897f8d5c6f754004b6&url=${locationUrl}`;
    const {
      data: { locations: top_locations = [] }
    } = await Request.get(location_proxy);

    Logger(`_Starting JOB Deal Finder_ at ${dayjs().format("DD-MM hh:mm")}`);

    const top_locations_foreign = top_locations
      .filter(location => location.country.id !== "IN")
      .map(location => ({
        name: location.name,
        code: location.code,
        country: location.country,
        continent: location.continent,
        region: location.region
      }));

    console.log("Sourav Logging:::: Top Loaction DS", top_locations_foreign[0]);
    const arrayToStringDest = R.pipe(
      R.reduce((acc, value) => acc + value.code + ",", ""),
      R.trim,
      R.replace(/,\s*$/, "")
    );
    const fly_from = "BLR,CCU,MAA,BOM,COK,DEL,GAU,ATQ,HYD,BBI";
    const fly_to = arrayToStringDest(top_locations_foreign);
    const date_from = dayjs()
      .add(1, "day")
      .format("DD/MM/YYYY");
    const date_to = dayjs()
      .add(10, "month")
      .format("DD/MM/YYYY");

    const searchURL = `https://api.skypicker.com/flights?fly_from=${fly_from}&fly_to=${fly_to}&v=3&date_from=${date_from}&date_to=${date_to}&nights_in_dst_from=3&nights_in_dst_to=10&one_for_city=1&partner=picky&curr=INR`;
    const searchurl_proxy = `http://api.scraperapi.com?api_key=09f8e0c1461b2d897f8d5c6f754004b6&url=${searchURL}`;
    const {
      data: { data: search_results = [] }
    } = await Request.get(searchurl_proxy);

    const query_results = search_results.map(result => {
      const {
        id,
        countryFrom,
        countryTo,
        dTimeUTC,
        cityFrom,
        cityTo,
        price,
        nightsInDest,
        deep_link,
        flyFrom,
        flyTo,
        route
      } = result;
      const ret_index = route.length > 3 ? route.length - 2 : route.length - 1;
      const res_search = {
        id,
        countryFrom,
        countryTo,
        dTimeUTC,
        cityFrom,
        cityTo,
        price,
        nightsInDest,
        deep_link,
        icao_from: flyFrom,
        icao_to: flyTo,
        dtimetrip1: route[0].dTimeUTC,
        dtimetrip2: route[ret_index].dTimeUTC
      };
      const location_data = top_locations_foreign.find(location => {
        return location.country.code === res_search.countryTo.code;
      });
      const final_result = {
        ...res_search,
        continent: R.pathOr(null, ["continent"], location_data),
        region: R.pathOr(null, ["region"], location_data)
      };
      return final_result;
    });

    console.log(query_results);
    // await destroy({ dbName: "deals", truncate: true });
    const cheappr_deals = query_results
      .filter(result => filterDeals(result))
      .map(result => {
        const deal_country = result.countryTo.code;
        const deal_continent = result.continent.id;
        const deal_region = result.region.id;
        // First We Check Country
        const findByCountry = priceListNew.find(
          price => price.country === deal_country
        );
        if (findByCountry)
          return {
            ...result,
            usual_price: findByCountry.usual_price
          };

        return {
          ...result,
          usual_price: 10000
        };
      });

    const deals_data = cheappr_deals.map(deal_data => ({
      deal_data
    }));

    await bulkInsert({
      dbName: "deals",
      data: deals_data
    });

    console.log(
      "Sourav Logging:::: Saved Deals on ",
      dayjs().format("DD/MM/YYYY")
    );

    // Save Deals
  } catch (error) {
    console.log("Cannot Find Deals from KIWI", error);
    Logger("Cannot find Deals from Kiwi----> *Error Occured*");
  }
};

const filterDeals = result => {
  const deal_country = result.countryTo.code;
  const price = result.price;

  // First We Check Country
  const findByCountry = priceListNew.find(
    price => price.country === deal_country
  );
  if (findByCountry && price <= findByCountry.cut_off) return true;

  // Second we check Region
  // Default Case--- An price below 10000 INR is good for now
  if (price < 10000) return true;
  return false;
};

module.exports = {
  dealKiwi
};
