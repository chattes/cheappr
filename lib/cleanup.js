const { rawquery, creditCoins, destroy } = require("../model");
const { sequelize, Sequelize } = require("./database");
const { Op } = Sequelize;

const RAWQUERYALERTS = `
select id, users from alerts WHERE current_date > ALL (dateofdeparture);
`;

const cleanUp = async () => {
  rawquery(RAWQUERYALERTS, { type: sequelize.QueryTypes.SELECT }).then(
    async ([results, metadata]) => {
      try {
        console.log(results);
        for (let result of results) {
          const { users } = result;
          for (let email of users) {
            await creditCoins(email);
          }
        }

        const ids = results.map(result => result.id);
        destroy({
          dbName: "alerts",
          where: {
            id: {
              [Op.in]: ids
            }
          }
        });
      } catch (error) {
        console.log(error);
      }
    }
  );
};

module.exports = {
  cleanUp
};
