const { insert, read } = require("../model");
const dayjs = require("dayjs");
const { APP_CONSTANTS } = require("../lib/constants");
const { Op } = require("sequelize");
const DEAL_USERS_NUMBER = 10;
const DEAL_USERS_NUMBER_FREE = 30;
const mailjet = require("node-mailjet").connect(
  process.env.MAILJET_API,
  process.env.MAILJET_SECRET
);

const publishDealsForUsers = async ({
  cityFrom,
  cityTo,
  countryFrom,
  countryTo,
  link,
  nights,
  price,
  usual_price,
  premium,
  notes,
  image_url,
  when,
  airlines
}) => {
  let users = [];

  if (premium) {
    users = await choosePremUsersForDeal();
  } else {
    users = await getAllUsersForDeal();
  }
  const email_users = users.map(user => user.email);
  saveDeals({
    cityFrom,
    cityTo,
    countryFrom,
    countryTo,
    link,
    nights,
    price,
    usual_price,
    users_sent: email_users,
    premium,
    notes,
    image_url,
    when,
    airlines
  });
  emailDeals({
    cityFrom,
    cityTo,
    countryFrom,
    countryTo,
    link,
    nights,
    price,
    usual_price,
    users_sent: users,
    premium,
    notes,
    image_url,
    when,
    airlines
  });
};

const saveDeals = ({
  cityFrom,
  cityTo,
  countryFrom,
  countryTo,
  link,
  nights,
  price,
  usual_price,
  users_sent,
  premium,
  notes,
  image_url,
  when,
  airlines
}) => {
  const data = {
    cityFrom,
    cityTo,
    countryFrom,
    countryTo,
    link,
    nights,
    price,
    usual_price,
    users_sent,
    premium,
    notes,
    image_url,
    when,
    airlines,
    valid_from: dayjs().toDate(),
    valid_to: dayjs()
      .add(1, "day")
      .toDate()
  };

  insert("published_deals", data);
};

const emailDeals = ({
  cityFrom,
  cityTo,
  countryFrom,
  countryTo,
  link,
  nights,
  price,
  usual_price,
  users_sent,
  premium,
  notes,
  image_url,
  when,
  airlines
}) => {
  if (users_sent.length == 0) return;
  const mailDealUsers = users_sent.map(user => ({
    Email: user.email,
    Name: user.firstName
  }));

  let subject_mail = `✈️ ${nights} nights in ${countryTo} for INR ${price}`;
  const request = mailjet.post("send", { version: "v3.1" }).request({
    Messages: users_sent.map(user => {
      return {
        From: {
          Email: "travel@cheappr.io",
          Name: "Cheappr"
        },
        To: [
          {
            Email: user.email,
            Name: user.firstName
          }
        ],
        TemplateID: 908177,
        TemplateLanguage: true,
        Subject: subject_mail,
        Variables: {
          cityfrom: cityFrom,
          cityto: cityTo,
          link: link,
          countryto: countryTo,
          price: price,
          usual_price: usual_price,
          nights: nights,
          firstName: user.firstName,
          notes: notes,
          when,
          airlines
          // url: image_url
        }
      };
    })
  });
  request
    .then(result => {
      console.log(result.body);
    })
    .catch(err => {
      console.log(err.statusCode);
    });
};

const choosePremUsersForDeal = async () => {
  const where = {
    membershipType: APP_CONSTANTS.MEMBER_PREMIUM,
    lastDeal: {
      [Op.lt]: dayjs()
        .toDate()
        .add(2, "day")
    }
  };

  const attributes = ["email", "firstName"];

  const usersModel = await read({
    dbName: "usersForDeal",
    where,
    limit: DEAL_USERS_NUMBER,
    attributes
  });

  const users = usersModel.map(user => user.get({ plain: true }));
  updateLastDealTimeForSelectedUsers(usersModel);
  return users;
};

const updateLastDealTimeForSelectedUsers = models => {
  models.forEach(model => {
    model.update({
      lastDeal: dayjs().toDate()
    });
  });
};
const getAllUsersForDeal = async () => {
  const where = {
    lastDeal: {
      [Op.lt]: dayjs().add(5, "hour")
    }
  };

  const attributes = ["email", "firstName"];

  const usersModel = await read({
    dbName: "usersForDeal",
    where,
    limit: DEAL_USERS_NUMBER_FREE,
    attributes
  });

  const users = usersModel.map(user => user.get({ plain: true }));
  updateLastDealTimeForSelectedUsers(usersModel);
  return users;
};

module.exports = {
  publishDealsForUsers
};
