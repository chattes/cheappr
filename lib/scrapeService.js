const puppeteer = require("puppeteer");
const cheerio = require("cheerio");

const scrapeURL = async () => {
  try {
    const browser = await puppeteer.connect({
      browserWSEndpoint: `ws://browserless:3000?--no-sandbox=true`
    });
    const page = await browser.newPage();
    await page.goto("https://example.com");
    await page.screenshot({ path: "example.png" });
    await browser.close();
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  scrapeURL
};
