const path = require("path");
const Good = require("good");
const Swagger = require("hapi-swagger");
const Inert = require("inert");
const Vision = require("vision");
const Pack = require("../package.json");
// const config = require('../config.js');
const moment = require("moment");

const logSqueezeArgs = [
  {
    log: "*",
    response: "*",
    request: "*",
    "request-internal": "*"
  }
];

const plugins = [Inert, Vision];
plugins.push({
  plugin: Good,
  options: {
    reporters: {
      console: [
        {
          module: "good-squeeze",
          name: "Squeeze",
          args: logSqueezeArgs
        },
        {
          module: "good-console",
          args: [
            {
              format: "HH:mm:ss DD.MM.YYYY"
            }
          ]
        },
        "stdout"
      ]
      // file: [
      //   {
      //     module: "good-squeeze",
      //     name: "Squeeze",
      //     args: logSqueezeArgs
      //   },
      //   {
      //     module: "good-squeeze",
      //     name: "SafeJson"
      //   },
      //   {
      //     module: "rotating-file-stream",
      //     args: [
      //       "log",
      //       {
      //         interval: "1d",
      //         compress: "gzip",
      //         path: "./logs"
      //       }
      //     ]
      //   }
      // ]
    }
  }
});

plugins.push({
  plugin: require("hapi-router"),
  options: {
    routes: "./routes/**/*.js",
    prefix: "/api"
  }
});

plugins.push({
  plugin: Swagger,
  options: {
    documentationPage: true, // boolean to enable/disable Swagger
    info: {
      title: "API Documentation",
      version: Pack.version
    },
    jsonEditor: true
  }
});

plugins.push({
  plugin: require("hapi-auth-jwt2")
});

plugins.push({
  plugin: require("hapi-pagination"),
  options: {
    // enabled: false,
    meta: {
      baseUri: ""
    },
    routes: {
      include: []
    }
  }
});

const registerPlugins = async server => {
  await server.register(plugins);
};
module.exports = {
  registerPlugins
};
