const { read } = require("../model");
const { sequelize, Sequelize } = require("../lib/database");
const Op = Sequelize.Op;
const getIATACodes = async query => {
  if (!query) return;

  const attributes = ["name", "code"];
  const where = {
    country_code: "IN",
    [Op.or]: [
      {
        code: {
          [Op.iLike]: `%${query}%`
        }
      },
      {
        name: {
          [Op.iLike]: `%${query}%`
        }
      }
    ]
  };
  const readResults = await read({ dbName: "iatacodes", where, attributes });
  console.log(readResults);
  return readResults.map(result => ({
    code: result.code,
    name: result.name
  }));
};

module.exports = {
  getIATACodes
};
