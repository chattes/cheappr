const Axios = require("axios");
const fs = require("fs").promises;
const R = require("ramda");
const dayjs = require("dayjs");
const Boom = require("boom");
const { destroy, bulkInsert, read } = require("../model");
const { Logger } = require("../utils/logger");
const { tweet, shouldTweet } = require("../utils/twitter");
const { priceIndicator } = require("../Rules/priceList");
const CountryCodes = require("../config/country.json");
const priceList = require("../config/pricelistnew.json");
let browser;

const Request = Axios.create({
  // baseURL: "https://api.skypicker.com",
  timeout: 20000
});

const getCountryCode = name => {
  let code;

  for (let countrycode in CountryCodes) {
    if (name.toLowerCase() === CountryCodes[countrycode].toLowerCase()) {
      code = countrycode;
      break;
    }
  }
  if (!code) {
    code = "ZZ";
    console.error("Country Code not found for Country:::", name);
  }
  return code;
};

const dealMomondo = async from => {
  try {
    Logger(
      `_Starting JOB Deal Finder MOMONDO_ at ${dayjs().format("DD-MM hh:mm")}`
    );

    const searchURL = `https://www.momondo.in/s/horizon/exploreapi/elasticbox?airport=${from}&v=1&stopsFilterActive=false&duration=&budget=40000&zoomLevel=4`;
    const searchurl_proxy = `http://api.scraperapi.com?api_key=09f8e0c1461b2d897f8d5c6f754004b6&url=${searchURL}`;
    const {
      data: { destinations, origin }
    } = await Request.get(searchurl_proxy);

    //   filter out all Domestic Flights

    const destinations_foreign = destinations.filter(
      destination => destination.country.name !== "India"
    );

    //   console.log("Sourav Logging:::: Deals Momondo", CountryCodes);
    const query_results = destinations_foreign.map(destination => ({
      countryFrom: {
        code: "IN",
        name: "India"
      },
      countryTo: {
        code: getCountryCode(destination.country.name),
        name: destination.country.name
      },
      cityFrom: origin.cityName,
      cityTo: destination.city.name,
      icao_from: origin.shortName,
      icao_to: destination.airport.shortName,
      price: destination.flightInfo.price,
      dTimeUTC: dayjs(destination.departd.split("-")[0]).unix(),
      dtimetrip1: dayjs(destination.departd.split("-")[0]).unix(),
      dtimetrip2: dayjs(destination.returnd.split("-")[0]).unix(),
      nightsInDest: dayjs(destination.returnd.split("-")[0]).diff(
        dayjs(destination.departd.split("-")[0]),
        "day"
      ),
      deep_link: `https://momondo.in${destination.clickoutUrl}`
    }));
    const cheappr_results = query_results
      .filter(res => res.nightsInDest > 6 && res.nightsInDest < 20)
      .filter(res => res.price < 60000);
    console.log("Sourav Logging:::: All Flights", cheappr_results);

    const cheappr_deals = cheappr_results
      .filter(result => filterDeals(result))
      .map(deal => {
        const priceMatched = priceList.find(
          price => price.country === deal.countryTo.code
        );
        return {
          ...deal,
          usual_price: priceMatched.usual_price || 0
        };
      });

    console.log("Sourav Logging:::: ", cheappr_deals);
    const deals_data = cheappr_deals.map(deal_data => ({
      deal_data
    }));

    await bulkInsert({
      dbName: "deals",
      data: deals_data
    });
  } catch (error) {
    console.log("Cannot Find Deals from Momondo", error);
    Logger("Cannot find Deals from Momondo----> *Error Occured*");
  }
};

const filterDeals = result => {
  const deal_country = result.countryTo.code;
  const price = result.price;
  // First We Check Country
  const findByCountry = priceList.find(price => price.country === deal_country);
  if (findByCountry && price <= findByCountry.cut_off) return true;
  // Default Case--- An price below 10000 INR is good for now
  if (price < 10000) return true;
  return false;
};

module.exports = {
  dealMomondo
};
