const RapidAPI = require("rapidapi-connect");
const { destroy, bulkInsert } = require("../model");
const rapid = new RapidAPI(
  "default-application_579888b2e4b0874aaf5e1b3c",
  "4d7cc74a-249d-4866-a9bd-767eb4bf3edd"
);
const { Logger } = require("../utils/logger");
const errorText = `
      > An **ERROR** has occured while loading IATA Codes
      `;
const infoText = `
      **Scheduled Job to Load IATA Codes Starting**
      `;
const successText = `
      **Scheduled Job to Load IATA Codes Completed Succesfully**
      `;
const loadcodes = () => {
  Logger(infoText);
  rapid
    .call("IATACodes", "getCities", {
      apiKey: process.env.RAPID_API_KEY
    })
    .on("success", async payload => {
      /*YOUR CODE GOES HERE*/

      try {
        if (payload.length > 0) {
          const { response } = payload[0];

          await destroy({ dbName: "iatacodes" });
          await bulkInsert({ dbName: "iatacodes", data: response });
          Logger(successText);
          console.log(`Loaded IATA Codes on ${Date()}`);
        }
      } catch (error) {
        Logger(errorText);
        console.log("Cannot load IATA Codes");
      }
    })
    .on("error", payload => {
      /*YOUR CODE GOES HERE*/
      Logger(errorText);
      console.log("Cannot Load Codes");
    });
};

module.exports = {
  loadcodes
};
