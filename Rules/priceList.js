const priceIndicator = [
  {
    country: "AU",
    usual_price: 60000,
    cut_off: 26000
  },
  {
    country: "CA",
    usual_price: 60000,
    cut_off: 50000
  },
  {
    country: "US",
    usual_price: 65000,
    cut_off: 50000
  },
  {
    country: "RU",
    usual_price: 35000,
    cut_off: 25000
  },
  {
    country: "AT",
    usual_price: 40000,
    cut_off: 20000
  },
  {
    country: "BY",
    usual_price: 40000,
    cut_off: 20000
  },
  {
    country: "BE",
    usual_price: 50000,
    cut_off: 20000
  },
  {
    country: "BG",
    usual_price: 50000,
    cut_off: 20000
  },
  {
    country: "DK",
    usual_price: 40000,
    cut_off: 20000
  },
  {
    country: "FR",
    usual_price: 40000,
    cut_off: 25000
  },
  {
    country: "DE",
    usual_price: 45000,
    cut_off: 25000
  },
  {
    country: "GR",
    usual_price: 40000,
    cut_off: 25000
  },
  {
    country: "IT",
    usual_price: 40000,
    cut_off: 20000
  },

  {
    country: "SG",
    usual_price: 15000,
    cut_off: 10000
  },
  {
    country: "TH",
    usual_price: 15000,
    cut_off: 10000
  },
  {
    country: "VN",
    usual_price: 15000,
    cut_off: 10000
  },
  {
    country: "MY",
    usual_price: 15000,
    cut_off: 10000
  },
  {
    country: "KR",
    usual_price: 30000,
    cut_off: 15000
  },
  {
    country: "CN",
    usual_price: 30000,
    cut_off: 12000
  },
  {
    country: "JP",
    usual_price: 30000,
    cut_off: 20000
  },
  {
    country: "IL",
    usual_price: 30000,
    cut_off: 20000
  },
  {
    country: "JO",
    usual_price: 30000,
    cut_off: 20000
  },
  {
    country: "MV",
    usual_price: 15000,
    cut_off: 10000
  },
  {
    country: "KR",
    usual_price: 30000,
    cut_off: 20000
  },
  {
    country: "NZ",
    usual_price: 50000,
    cut_off: 30000
  },
  {
    country: "TR",
    usual_price: 40000,
    cut_off: 20000
  },
  {
    country: "asia",
    usual_price: 30000,
    cut_off: 15000
  },
  {
    country: "oceania",
    usual_price: 100000,
    cut_off: 30000
  },
  {
    country: "south-america",
    usual_price: 200000,
    cut_off: 80000
  },
  {
    country: "Carribean",
    usual_price: 200000,
    cut_off: 80000
  },
  {
    // General Rest of Europe
    country: "north-america",
    usual_price: 100000,
    cut_off: 40000
  },
  {
    // General Rest of Europe
    country: "europe",
    usual_price: 50000,
    cut_off: 25000
  },
  {
    country: "africa",
    usual_price: 40000,
    cut_off: 25000
  },
  {
    country: "southern-asia",
    usual_price: 20000,
    cut_off: 12000
  },
  {
    country: "south-eastern-asia",
    usual_price: 25000,
    cut_off: 15000
  },
  {
    country: "western-asia",
    usual_price: 30000,
    cut_off: 15000
  },
  {
    country: "australasia",
    usual_price: 50000,
    cut_off: 30000
  },
  {
    country: "central-europe",
    usual_price: 70000,
    cut_off: 35000
  },
  {
    country: "northern-europe",
    usual_price: 70000,
    cut_off: 35000
  },
  {
    country: "eastern-asia",
    usual_price: 50000,
    cut_off: 25000
  },
  {
    country: "western-europe",
    usual_price: 50000,
    cut_off: 30000
  },
  {
    country: "northern-america",
    usual_price: 100000,
    cut_off: 50000
  }
];

module.exports = {
  priceIndicator
};
