const Db = require("../lib/database");
const { sequelize, Sequelize } = Db;
const Boom = require("boom");

const IATACodes = sequelize.define("iatacode", {
  code: { type: Sequelize.STRING, allowNull: false, unique: "codes" },
  country_code: { type: Sequelize.STRING, allowNull: false, unique: "codes" },
  name: { type: Sequelize.STRING, allowNull: false, unique: "codes" }
});

const User = sequelize.define("user", {
  email: { type: Sequelize.STRING, allowNull: false, primaryKey: true },
  firstName: { type: Sequelize.STRING },
  lastName: { type: Sequelize.STRING },
  fullName: { type: Sequelize.STRING },
  profilePic: { type: Sequelize.STRING },
  lastDeal: { type: Sequelize.DATEONLY },
  membershipType: { type: Sequelize.STRING },
  validFrom: { type: Sequelize.DATEONLY },
  validTill: { type: Sequelize.DATEONLY },
  role: { type: Sequelize.STRING, defaultValue: "USER" }
});

const PublishedDeals = sequelize.define("published_deals", {
  cityFrom: { type: Sequelize.STRING },
  cityTo: { type: Sequelize.STRING },
  countryFrom: { type: Sequelize.STRING },
  countryTo: { type: Sequelize.STRING },
  link: { type: Sequelize.TEXT },
  nights: { type: Sequelize.INTEGER },
  price: { type: Sequelize.INTEGER },
  usual_price: { type: Sequelize.INTEGER },
  users_sent: { type: Sequelize.ARRAY(Sequelize.STRING) },
  users_booked: { type: Sequelize.ARRAY(Sequelize.STRING) },
  premium: { type: Sequelize.BOOLEAN },
  valid_from: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
  valid_to: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
  notes: { type: Sequelize.TEXT },
  image_url: { type: Sequelize.TEXT },
  when: { type: Sequelize.TEXT },
  airlines: { type: Sequelize.TEXT }
});

const Transactions = sequelize.define("transactions", {
  transaction_detail: { type: Sequelize.JSON }
});
User.hasMany(Transactions);

const Deals = sequelize.define("deals", {
  deal_data: { type: Sequelize.JSON }
});

const createModels = async () => {
  await sequelize.authenticate();
  console.log(">>>>>>>Database Connected<<<<<<<<<<");
  console.log(">>>>>>>Create Tables<<<<<<<<<<");
  await IATACodes.sync();
  console.log(">>>>>>>Table IATACodes Created<<<<<<<");
  await User.sync();
  console.log(">>>>>>>Table User Created<<<<<<<");
  await Transactions.sync();
  console.log(">>>>>>>Table Transactions Created<<<<<<<");
  await Deals.sync();
  console.log(">>>>>>>Table Deals Created<<<<<<<");
  await PublishedDeals.sync();
  console.log(">>>>>>>>>Table Published deals Created<<<<<<<<");
};

const insert = async (dbname, data, defaults = {}) => {
  switch (dbname) {
    // return await Alerts.findOrCreate({ where });
    case "user":
      const [user, created] = await User.findOrCreate({
        where: data,
        defaults
      });

      if (user) {
        const UserObj = user.get({
          plain: true
        });

        return {
          ...UserObj,
          created: created
        };
      } else {
        throw new Error("Error creating User in Tables");
        return;
      }
    case "transactions":
      return Transactions.create(data);
    case "published_deals":
      return PublishedDeals.create(data);
    default:
      throw new Error("Database not found");
  }
};

const update = async (dbname, data, where) => {
  switch (dbname) {
    default:
      throw new Error("Database not found");
  }
};

const destroy = async ({ dbName, where, truncate = false }) => {
  switch (dbName) {
    case "iatacodes":
      (await where)
        ? IATACodes.destroy({ force: true, where })
        : IATACodes.destroy({ force: true, truncate: true });
      break;
  }
};

const bulkInsert = async ({ dbName, data }) => {
  if (data.length === 0) throw new Error("No Data passed for Creation");
  switch (dbName) {
    case "alerts":
      return;
    case "deals":
      await Deals.bulkCreate(data);
      break;
    case "iatacodes":
      await IATACodes.bulkCreate(data);
      break;
  }
};

const read = async ({
  dbName,
  where,
  attributes,
  order,
  limit,
  offset,
  group
}) => {
  switch (dbName) {
    case "iatacodes":
      const queryResults = await IATACodes.findAll({
        attributes,
        where
      });
      return queryResults;
    case "user":
      const userRes = await User.findOne({ where });
      return userRes;
    case "usersForDeal":
      const userDealRes = await User.findAll({ where, limit, attributes });
      return userDealRes;
    case "countryCodes":
      const countryCodes = await Countries.findOne({ where });
      return countryCodes;
    case "deals":
      const deals = await Deals.findAndCountAll({
        where,
        order,
        limit,
        offset,
        group
      });
      return deals;
    case "published_deals":
      const published_deals = await PublishedDeals.findAndCountAll({
        where,
        order,
        limit,
        offset,
        group
      });
      return published_deals;
  }
};

const readById = async ({ dbName, id }) => {
  switch (dbName) {
    case "alerts":
      return await Alerts.findById(id);
    case "iatacodes":
      throw Boom.notImplemented();

    case "user":
      throw Boom.notImplemented();
  }
};

const rawquery = async query => await sequelize.query(query);

module.exports = {
  createModels,
  insert,
  update,
  rawquery,
  destroy,
  bulkInsert,
  read,
  readById
};
